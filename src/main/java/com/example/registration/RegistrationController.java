package com.example.registration;


import com.example.user.User;
import lombok.RequiredArgsConstructor;

import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@CrossOrigin(origins = "http://localhost:4200")
public class RegistrationController {

    private final RegistrationService registrationService;


    @PostMapping("/register")
    public User createUser(@RequestBody RegistrationDTO registrationDTO) {
        return registrationService.createUser(registrationDTO);
    }

    @GetMapping("/activate")
    public String activateAccount(@RequestParam("code") String verificationCode) {
        return registrationService.activateAccount(verificationCode);
    }

}

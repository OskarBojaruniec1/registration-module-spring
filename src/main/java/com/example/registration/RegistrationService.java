package com.example.registration;

import com.example.mailSender.EmailSender;
import com.example.user.User;
import com.example.user.UserRepository;
import lombok.RequiredArgsConstructor;
import net.bytebuddy.utility.RandomString;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class RegistrationService {

    private final UserRepository userRepository;
    private final EmailSender emailSender;

    public User createUser(RegistrationDTO registrationDTO) {
        User user = new User();

        user.setLogin(registrationDTO.getLogin());
        user.setPassword(registrationDTO.getPassword());
        user.setEmail(registrationDTO.getEmail());
        user.setActivated(false);
        user.setVerificationCode(RandomString.make(64));
        sendVerificationMail(user.getEmail(), user.getVerificationCode());

        return userRepository.save(user);
    }

    public void sendVerificationMail(String email, String verificationCode) {
        emailSender.sendMail(email, verificationCode);
    }

    public String activateAccount(String verificationCode) {
        User user = userRepository.findByVerificationCode(verificationCode);
        user.setActivated(true);
        userRepository.save(user);
        return "account activated!";
    }
}
package com.example.registration;

import lombok.Data;
import lombok.Getter;

@Data
@Getter
public class RegistrationDTO {

    private String login;
    private String password;
    private String email;
}

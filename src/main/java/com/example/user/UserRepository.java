package com.example.user;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    @Query("select u from User u where verificationCode = ?1")
    User findByVerificationCode(String verificationCode);

    @Query("select u from User u where login =?1 and password =?2")
    User findByLoginAndPassword(String login, String password);
}

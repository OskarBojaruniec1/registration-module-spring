package com.example.user;


import lombok.Getter;

import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String login;
    private String password;
    private String email;
    @Column(name = "is_activated")
    private boolean isActivated;
    @Column(name = "verification_code")
    private String verificationCode;
}

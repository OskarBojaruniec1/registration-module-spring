package com.example.mailSender;

import lombok.RequiredArgsConstructor;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;


@RequiredArgsConstructor
@Component
public class EmailSender {

    private final JavaMailSender mailSender;


    public void sendMail(String userMail, String verificationCode) {

        SimpleMailMessage message = new SimpleMailMessage();

        message.setFrom("oskar.bojaruniec1@gmail.com");
        message.setTo(userMail);
        message.setSubject("Activate account");
        String URL = "http://localhost:8080/";
        message.setText(URL + "activate?code=" + verificationCode);

        mailSender.send(message);
    }

}

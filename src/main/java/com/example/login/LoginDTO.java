package com.example.login;

import lombok.Data;

@Data
public class LoginDTO {

    private String login;
    private String password;
}

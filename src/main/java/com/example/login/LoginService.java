package com.example.login;

import com.example.user.User;
import com.example.user.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class LoginService {

    private final UserRepository userRepository;


    public String login(LoginDTO loginDTO) {
        User user = userRepository.findByLoginAndPassword(loginDTO.getLogin(), loginDTO.getPassword());
        if (user != null && isActivated(user.isActivated())){
            return "Logged in";
        }
        else {
            return "Wrong login or password";
        }

    }

    private boolean isActivated(Boolean isActivated) {
        return isActivated;
    }
}
